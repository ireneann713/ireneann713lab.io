import React, { useState, useEffect, Profiler } from "react";
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Button from 'react-bootstrap/Button'
import Intel from "../../editable-stuff/download.png"
import coursera from "../../editable-stuff/coursera.png"
import deep from "../../editable-stuff/deep.png"
import {
    aboutHeading,
    aboutDescription,
    showInstaProfilePic,
    instaLink,
    instaUsername,
    instaQuerry,
  } from "../../editable-stuff/configurations.json";
  const divStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  };
const Interests = () => {
  return (
    <div id="interests" className="jumbotron jumbotron-fluid m-0" style={{backgroundColor:'white'}}>
      <div className="container container-fluid">
        <div className="row">
            <div className="col d-lg-inline align-self-center">
            <h1 className="display-4 pb-4 text-center">Certifications</h1>
            {/* <p className="lead text-center">
              Outside of software engineering, I love to meditate, read, and play soccer!
  </p><br/>*/ }
            <div className="row" style={divStyle}>
                <div className="col text-center" >
                <Card style={{ width: '15rem',height: '26rem' }}>
  <Card.Img variant="top"  src={Intel} />
  <Card.Body>
  <br></br>
    <br></br>
    <br></br>
    <br></br>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <Card.Title>Introduction to OpenCL on FPGAs</Card.Title>
    
    <Card.Text>
      
    </Card.Text>
    <Button variant="primary" href="https://coursera.org/share/dd9173fb56db80989b023d63df9d8505">View</Button>
    
  </Card.Body>
</Card>
                </div>
                <div className="col text-center">
                <Card style={{ width: '15rem',height:'26rem' }}>
  <Card.Img variant="top" src={coursera} />
  <Card.Body>
  <br></br>
    <Card.Title>Image Processing with Python</Card.Title>
    <Card.Text>
      
    </Card.Text>
    <Button variant="primary" href="https://coursera.org/share/38ee2e437ff9049f297c53a3ac59786e">View</Button>
    
  </Card.Body>
</Card>
                
                </div>
                
                <div className="col text-center">
                <Card style={{ width: '15rem',height: '26rem' }}>
  <Card.Img variant="top" src={deep} />
  <Card.Body>
  <br></br>
    <br></br>
    <br></br>
    <br></br>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <Card.Title>Natural Language Processing in Tensorflow</Card.Title>
    <Card.Text>
      
    </Card.Text>
    <Button variant="primary" href="https://www.coursera.org/account/accomplishments/verify/FLVRLUUQBS4Y?utm_source=link&utm_medium=certificate&utm_content=cert_image&utm_campaign=pdf_header_button&utm_product=course">View</Button>
    
  </Card.Body>
</Card>
                </div>
                <div className="col text-center">
                <Card style={{ width: '15rem', height:'10 rem'}}>
  <Card.Img variant="top" src={coursera} />
  <Card.Body>
    <Card.Title>Detecting COVID-19 with Chest X-Ray using
PyTorch</Card.Title>
    <Card.Text>
      
    </Card.Text>
    <Button variant="primary" href="https://coursera.org/share/981e6490d5d6a60e74ae15856220e867">View</Button>
    
  </Card.Body>
</Card>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  );
};
export default Interests;
