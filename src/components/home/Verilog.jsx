import React, { useState, useEffect, Profiler } from "react";
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Button from 'react-bootstrap/Button'
import Intel from "../../editable-stuff/download.png"
import halfadder from "../../editable-stuff/half_adder_eda.png"
import fulladder from "../../editable-stuff/fulladder.png"
import dff from "../../editable-stuff/dff.png"
import deep from "../../editable-stuff/deep.png"
import pe from "../../editable-stuff/Screenshot__319_.png"
import pez from "../../editable-stuff/Screenshot__321_.png"
import cou from "../../editable-stuff/Screenshot__326_.png"
import sr from "../../editable-stuff/Screenshot__19_.png"

import {
    aboutHeading,
    aboutDescription,
    showInstaProfilePic,
    instaLink,
    instaUsername,
    instaQuerry,
  } from "../../editable-stuff/configurations.json";
  const divStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  };
const Verilog = () => {
  return (
    <div id="verilog" className="jumbotron jumbotron-fluid m-0" style={{backgroundColor:'white'}}>
      <div className="container container-fluid">
        <div className="row">
            <div className="col d-lg-inline align-self-center">
            <h1 className="display-4 pb-4 text-center">100 Days of verilog</h1>
            {/* <p className="lead text-center">
              Outside of software engineering, I love to meditate, read, and play soccer!
  </p><br/>*/ }
            <div className="row" style={divStyle}>
                <div className="col text-center" >
                <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 1</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Half Adder</Card.Subtitle>
    <Card.Img variant="top" src={halfadder} />
    <Card.Text>
    Learnt to create a Half Adder Circuit using Verilog and write testbench in verilog
    </Card.Text>
    
    <Card.Link href="https://www.edaplayground.com/x/WK84">View</Card.Link>
  </Card.Body>
</Card>
      
                </div>
                <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 2</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Full Adder</Card.Subtitle>
    <Card.Img variant="top" src={fulladder} />
    <Card.Text>
    Learnt to create a Full Adder Circuit using Verilog and write testbench in verilog
    </Card.Text>
    
    <Card.Link href="https://www.edaplayground.com/x/D6H9">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                  <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 3</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">D Flip Flop</Card.Subtitle>
    <Card.Img variant="top" src={dff} />
    <Card.Text>
    Learnt to design D Flipflop using Verilog and write testbench in verilog
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/B27A">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 4</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Priority Encoder</Card.Subtitle>
    <Card.Img variant="top" src={pe} />
    <Card.Text>
    Learnt to design Priority Encoder using Verilog and write testbench in verilog
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/nBjD">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 5</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Priority Encoder using casez</Card.Subtitle>
    <Card.Img variant="top" src={pez} />
    <Card.Text>
   Built a priority encoder for 8-bit inputs using casez
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/NDsq">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                  <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 6</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">4 bit counter </Card.Subtitle>
    <Card.Img variant="top" src={cou} />
    <Card.Text>
   Built a 4 bit counter
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/HwWk">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 7</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">SR Flip Flop </Card.Subtitle>
    <Card.Img variant="top" src={sr} />
    <Card.Text>
   Built a 4 bit counter
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/9Vif">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
                <div className="col text-center">
                 <Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Day 7</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">SR Flip Flop </Card.Subtitle>
    <Card.Img variant="top" src={sr} />
    <Card.Text>
   Built a 4 bit counter
    </Card.Text>
    
    <Card.Link href=" https://www.edaplayground.com/x/9Vif">View</Card.Link>
  </Card.Body>
</Card>
                
                </div>
               
               
            </div>
        </div>
        </div>
      </div>
    </div>
  );
};
export default Verilog;
