import React, { useState, useEffect, Profiler } from "react";
import Carousel from 'react-bootstrap/Carousel'


const Leadership = () => {
    return (
        <div id="leadership" className="jumbotron jumbotron-fluid m-0" style={{backgroundColor:'white'}}>
          <div className="container container-fluid">
            <div className="row"> 
                <div className="col d-lg-inline align-self-center">
                <h1 className="display-4 mb-3 text-center">Leadership</h1>
                {/* <p className="lead text-center" style={{fontSize:'22px'}}>
                    I empower people to acheive their goals. And I absolutely love it.
                </p><br/><br/> */}
                <br/>
                <div className="row"> 
                <div className="col d-lg-inline align-self-center">
                  <p className="lead text-center">
                      Throughout my college career, I've been extremely grateful to have opportunities to lead my peers
                      and make an impact on the Engineering community. In my role as the <b>Chairperson of Women In Engineering of IEEE SB RIT </b>, I cultivated a culture of 
                      inclusivity, collaboration, and ambition through leading new-member recruitment and 
                      community building events.
                    
                      Outside of these roles, I worked as the <b>Web Development Mentor</b> of ORE Training Program Of IEEE Kochi HUB.
                 </p>  
              </div>
              
                
              </div>
            </div>
          </div>
      </div>
      </div>
      );
    };

export default Leadership;