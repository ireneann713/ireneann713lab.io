import React, { useState, useEffect, Profiler } from "react";
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Button from 'react-bootstrap/Button'
import Intel from "../../editable-stuff/download.png"
import halfadder from "../../editable-stuff/half_adder_eda.png"
import deep from "../../editable-stuff/deep.png"
import {
    aboutHeading,
    aboutDescription,
    showInstaProfilePic,
    instaLink,
    instaUsername,
    instaQuerry,
  } from "../../editable-stuff/configurations.json";
  const divStyle = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  };
const Verilog = () => {
  return (
    <div id="interests" className="jumbotron jumbotron-fluid m-0" style={{backgroundColor:'white'}}>
      <div className="container container-fluid">
        <div className="row">
            <div className="col d-lg-inline align-self-center">
            <h1 className="display-4 pb-4 text-center">100 days of Verilog</h1>
            {/* <p className="lead text-center">
              Outside of software engineering, I love to meditate, read, and play soccer!
  </p><br/>*/ }
            <div className="row" style={divStyle}>
                <div className="col text-center" >
                <Card style={{ width: '15rem',height: '26rem' }}>
  <Card.Img variant="top"  src={halfadder} />
  <Card.Body>
  <br></br>
    <br></br>
    <br></br>
    <br></br>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <p></p>
    <Card.Title>Day 1</Card.Title>
    
    <Card.Text>
      Learnt to create a Half Adder Circuit using Verilog and write testbench in verilog
    </Card.Text>
    <Button variant="primary" href="https://www.edaplayground.com/x/WK84">View</Button>
    
  </Card.Body>
</Card>
                </div>
               
            </div>
        </div>
        </div>
      </div>
    </div>
  );
};
export default Verilog;
