const ghpages = require("gh-pages");
const pathname = `${__dirname}/build`;
const repoURL = "https://gitlab.com/ireneann713/ireneann713.gitlab.io.git";

ghpages.publish(
  pathname,
  {
    branch: "master",
    repo: repoURL,
  },
  (err) => {
    if (err) console.log("ERROR: ", err);
    else console.log("PUBLISHED");
  }
);
